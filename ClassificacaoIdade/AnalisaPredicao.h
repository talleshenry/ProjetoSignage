#pragma once
#include <iostream>

using namespace std;

class AnalisaPredicao
{
	string box_text;

public:
	void analisePredicaoSexo(int predicao);
	void analisePredicaoIdade(int predicao);
	void analisePredicaoID(int predicao);
	string getDescricao();
	
};

