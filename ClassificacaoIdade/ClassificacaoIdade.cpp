// ClassificacaofaixaIdade.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "ShowVideo.h"
#include "LoadCSV.h"
#include "AnalisaPredicao.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;


bool showVideo(char sexo, int faixaIdade);

int _tmain(int argc, _TCHAR* argv[])
{
	
	// holds images and labels
	vector<Mat>* images;//vetor de imagens
	vector<int>* labelsIdade;//vetor de marca��o de idade
	vector<int>* labelsSexo;
	vector<int>* labelsID;

	string output_folder = ".";//sa�da
	int deviceId = 1;//n�mero da camera
	string fn_haar = "C:/opencv/sources/data/haarcascades/haarcascade_frontalface_alt_tree.xml";


	//carrega os modelos das imagens
	LoadCSV modelos;
	Ptr<FaceRecognizer> modelIdade = modelos.getModelIdade();//VERIFICAR ESSA FUN��O PARA DEFINIR UM 
	Ptr<FaceRecognizer> modelSexo = modelos.getModelSexo();
	Ptr<FaceRecognizer> modelID = modelos.getModelID();
	


	CascadeClassifier haar_cascade;
	haar_cascade.load(fn_haar);
	// Seleciona de qual dispositivo ir� capturar o v�deo
	VideoCapture cap(0);
	// checa se pode usar este dispositivo
	if (!cap.isOpened()) {
		cerr << "Capture Device ID " << deviceId << "cannot be opened." << endl;
		return -1;
	}

	// Vari�veis para serem trabalhadas na detec��o

	Mat frame; //armazena o frame atual do v�deo
	int predictionIdade; //estimativa de idade encontrada
	int predictionSexo; //sexo estimado
	int predictionID;//ID estimado

	//Mat original; //para salvar c�pia do frame e trabalhar com ele
	Mat gray;//salvar a c�pia em escala de cinza
	vector< Rect_<int> > faces;// vetor de faces encontradas
	//char timeStr[9];// armazenar a hora atual
	//char seg[1];//armazenar o caractere do segundo
	//int segundo; //armazena o segundo convertido para o tipo int
	int idImagem = 99;//ultimo id do banco de imagens
	int numeroDeFrame = 0;
	int sexoPresente[2];
	int faixaIdade[6];

	for (;;) {
		cap >> frame;
		numeroDeFrame++;
		
		// converte o frame corrente em escala de cinza:
		cvtColor(frame , gray, CV_BGR2GRAY);
		
		// Procura faces no frame
		haar_cascade.detectMultiScale(frame, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 100));
		
		//Size(Min, Max), Tamanho m�nimo ou m�ximo do objeto poss�vel. Objetos menores do que s�o ignorados.

		// At this point you have the position of the faces in
		// faces. Now we'll get the faces, make a prediction and
		// annotate it in the video. Cool or what?


		for (int i = 0; i < faces.size(); i++) {
			// processa cada face encontrada:
			Rect face_i = faces[i];
			// Crop the face from the image. So simple with OpenCV C++:
			Mat face = gray(face_i);
			//redimensiona a imagem para o padr�o das imagens salvas no banco de imagens
			Mat face_resized;
			cv::resize(face, face_resized, Size(200, 220), 1.0, 1.0, INTER_CUBIC);

			// Realiza uma predetermina��o das caracteristicas do rosto extra�do na imagem.
			predictionIdade = modelIdade->predict(face_resized);
			predictionSexo = modelSexo->predict(face_resized);
			predictionID = modelID->predict(face_resized);

			// desenha um ret�ngulo no rosto com as informa��es sobre idade e sexo
			rectangle(frame, face_i, CV_RGB(0, 255, 0), 1);
			if (predictionIdade != -1)
			{
				faixaIdade[predictionIdade]++;
			}
			if (predictionSexo != -1)
			{
				sexoPresente[predictionSexo]++;
			}
				
				//An�lise da predi��o verifica qual foi o sexo e a idade identificada pelo programa
				AnalisaPredicao predicao;
				predicao.analisePredicaoSexo(predictionSexo);
				predicao.analisePredicaoIdade(predictionIdade);

				if (predictionID != -1)//se o ID for identificada, ou seja, a predi��o for diferente de -1
				{
					predicao.analisePredicaoID(predictionID);//an�lise da predi��o do ID
				}
				else//se n�o for identificado, gera uma nova imagem e um novo treino � realizado.
				{

					idImagem++;
					vector<Mat> newImages;
					vector<int> newLabels;

					newImages.push_back(face_resized);
					newLabels.push_back(idImagem);

					imwrite(format("Images/5%dA29.JPG", idImagem), face_resized);
					modelID->update(newImages, newLabels);
				}//fim do else

				// Calculate the position for annotated text (make sure we don't
				// put illegal values in there):
				int pos_x = std::max(face_i.tl().x - 10, 0);
				int pos_y = std::max(face_i.tl().y - 10, 0);
				// And now put it into the image:


				putText(frame, predicao.getDescricao(), Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);

			}//fim do for

		//a cada 30 frames capturados verifica-se as quantidades de rostos encontrados e classificados por idades e g�neros dos rostos 
			if (numeroDeFrame >= 30)
			{
				char maiorSexo;
				int maiorIdade = -1;

				if (sexoPresente[0] < sexoPresente[1]){
					maiorSexo = 'F';
				}
				else
				{
					maiorSexo = 'M';
				}

				for (int i = 1; i <= 6; i++)
				{
					if (faixaIdade[maiorIdade] <= faixaIdade[i])
					{
						maiorIdade = i;
					}
				}//fim do for

				//chama a classe ShowVideo para mostrar o v�deo relacionado �s caracteristicas identificadas
				ShowVideo::iniciaVideo(maiorSexo, maiorIdade);///chama o v�deo

				//reinicia a contagem de frames
				numeroDeFrame = 0;
				sexoPresente[0] = 0;
				sexoPresente[1] = 0;

				for (int i = 0; i < 6; i++)
				{
					faixaIdade[i] = 0;
				}//fim do for
			}//fim do if

		// Mostra o resultado:
		imshow("face_recognizer", frame);
		// And display it:
		char key = (char)waitKey(20);
		// Exit this loop on escape:
		if (key == 27)

			break;
	}


	return 0;
}

