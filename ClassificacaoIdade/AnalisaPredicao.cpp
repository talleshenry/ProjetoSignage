#include "stdafx.h"
#include "AnalisaPredicao.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

/*==========================================================================
Esta classe faz a an�lise da predi��o, identificando quel foi o sexo, a idade
e o ID identificado, retornando um texto.
============================================================================*/

void AnalisaPredicao::analisePredicaoSexo(int predicao){

	if (predicao == 0){
		box_text += "Masculino";
	}
	else if (predicao == 1)
	{
		box_text += "Feminino";
	}
	else if (predicao == -1)
	{
		box_text += "??";
	}

}

void AnalisaPredicao::analisePredicaoIdade(int predicao){

	if (predicao == 0)
	{
		box_text += format("0-12");
	}
	else if (predicao == 1)
	{
		box_text += format("13-19");
	}
	else if (predicao == 2)
	{
		box_text += format("20-36");
	}
	else if (predicao == 3)
	{
		box_text += format("37-65");
	}
	else if (predicao == 4)
	{
		box_text += format("maior de 65 anos");
	}
	else if(predicao == -1){
		box_text = "----";
	}
}

void AnalisaPredicao::analisePredicaoID(int predicao){
	
	box_text += format(" %d", predicao);
}

string AnalisaPredicao::getDescricao(){

	return box_text;
}
