#include "stdafx.h"
#include "ShowVideo.h"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "ShowVideo.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;


void ShowVideo::iniciaVideo(char sexo, int faixaIdade){

	string nomeVideo;


	if (sexo == 'M'){
		switch (faixaIdade)
		{

		case 0://at� 12 anos
			nomeVideo = "video/M1.mp4";
			break;

		case 1://at� 19 anos
			nomeVideo = "video/M2.mp4";
			break;

		case 2://at� 36 anos
			nomeVideo = "video/M3.mp4";
			break;

		case 3://at� 65 anos
			nomeVideo = "video/M4.mp4";
			break;

		case 4://maior qye 65 anos
			nomeVideo = "video/M5.mp4";
			break;
		default:
			break;
		}
	}
	else if (sexo == 'F')
	{
		switch (faixaIdade)
		{

		case 0://at� 12 anos
			nomeVideo = "video/F1.mp4";
			break;

		case 1://at� 19 anos
			nomeVideo = "video/F2.mp4";
			break;

		case 2://at� 36 anos
			nomeVideo = "video/F3.mp4";
			break;

		case 3://at� 65 anos
			nomeVideo = "video/F4.mp4";
			break;

		case 4://maior que 65 anos
			nomeVideo = "video/F5.mp4";
			break;
		default:
			break;
		}
	}



	VideoCapture cap(nomeVideo); // open the video file for reading

	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "Cannot open the video file" << endl;
		return;
	}

	//cap.set(CV_CAP_PROP_POS_MSEC, 300); //start the video at 300ms

	double fps = cap.get(CV_CAP_PROP_FPS); //get the frames per seconds of the video

	cout << "Frame per seconds : " << fps << endl;

	namedWindow("Video", CV_WINDOW_NORMAL);
	setWindowProperty("Video", CV_WND_PROP_FULLSCREEN, CV_WINDOW_NORMAL);

	while (1)
	{
		Mat frame;

		bool bSuccess = cap.read(frame); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			break;
		}

		imshow("Video", frame); //show the frame in "Video" window

		if (waitKey(30) == 27) //wait for 'esc' key press for 30 ms. If 'esc' key is pressed, break loop
		{
			break;
		}
	}

}




