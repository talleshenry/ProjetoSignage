/*

	Esta classe faz com que seja treinada todas as imagens do banco de imagens, retornando os modelos para que serem utilizados para
	identificar o sexo, a idade e o ID das pessoas no ambiente.

	Created by Talles Henrique

*/

#include "stdafx.h"
#include "LoadCSV.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;


//M�TODO CONSTRUTOR DA CLASSE
LoadCSV::LoadCSV()
{
	// vetores que armazendam as images e as etiquetas (labels)
	// as etiquetas s�o as caracter�sticas atribu�das a cada imagem. A sequencia das imagens no vetor de imagens tem 
	//uma etiqueta correspondente na mesma posi��o dos vetores de etiquetas. Quando for treinada as imagens, ser� informada
	//sua respectiva etiqueta de idade, ID e sexo.
	static vector<Mat> images;//vetor de imagens
	static vector<int> labelsIdade;//vetor de etiqueta de idade
	static vector<int> labelsSexo;//vetor de etiqueta de sexo
	static vector<int> labelsID;//vetor de etiqueta de ID


	string line;//captar a linha do arquivo txt com o nome da imagem
	ifstream myfile("Images/csv1.txt");//carregando arquivo
	int aux = 0;//apenas para contar quantas imagens
	string idadeString;//vari�vel de apoio para extrair a idade do nome da imagem
	string sexoString;//vari�vel para extrair o sexo presente no nome da imagem ('A' para homem e 'B' para mulher)
	string IDString;//vari�vel para extrair o id de cada imagem do banco

	int intIdade;//Vari�vel de apoio para receber a string convertida para inteiro



	//Salvando as imagens e as marca��es dentro do vetor
	if (myfile.is_open())//abrindo o arquivo
	{
		while (getline(myfile, line))//enquanto tiver pr�xima linha dentro do arquivo fa�a
		{
			images.push_back(imread("Images/" + line, CV_LOAD_IMAGE_GRAYSCALE));//salve a imagem no vetor informando o seu endere�o


			//========================================================================================
			//Extrai o sexo de cada imagem e adiciona ao vector
			//========================================================================================
			sexoString = line[3];//pega a parte do nome do arquivo que cont�m a informa��o sobre o sexo

			if (sexoString == "A")//se for masculino, insere o valor 1 no vetor
			{
				labelsSexo.push_back(0);
			}
			else if (sexoString == "B")//se for feminino, insere o valor 2 no vetor
			{
				labelsSexo.push_back(1);
			}

			//=============================================================================
			//as pr�ximas linhas extraem a idade do nome da imagens e atribui ao vetor
			//=============================================================================
			idadeString = line[4];//pega o valor das dezenas
			idadeString += line[5];//pega o valor das unidades

			intIdade = stoi(idadeString);//converte a string idade para inteiro

			//verifica o intervalo de idade e adiciona uma marca��o
			if (intIdade >= 0 && intIdade <= 2){
				labelsIdade.push_back(0);
			}
			else if (intIdade >= 3 && intIdade <= 7)
			{
				labelsIdade.push_back(0);
			}
			else if (intIdade >= 8 && intIdade <= 12)
			{
				labelsIdade.push_back(0);
			}
			else if (intIdade >= 13 && intIdade <= 19)
			{
				labelsIdade.push_back(1);
			}
			else if (intIdade >= 20 && intIdade <= 36)
			{
				labelsIdade.push_back(2);
			}
			else if (intIdade >= 37 && intIdade <= 65)
			{
				labelsIdade.push_back(3);
			}
			else if (intIdade >= 66)
			{
				labelsIdade.push_back(4);
			}

			//==============================================================
			// Cria o vetor com o id dos usu�rios
			//==============================================================

			IDString = line[0];
			IDString += line[1];
			IDString += line[2];

			int ID = stoi(IDString);//converte para inteiro

			labelsID.push_back(ID);//insere no vetor

			aux++;//contador de imagens
		}
		myfile.close();
		cout << "total de imagens: " << aux << endl;//mostra o total de imagens
	}

	else cout << "Unable to open file";


	//========================================================================================
	//	as pr�ximas linhas criam e retornam um modelo, para cada vetor de etiquetas. isso faz com que o
	//programa compare uma imagem capturada pela camera com o modelo, e retorne uma predi��o informando qual
	//a poss�vel caracteristica (sexo, ID, idade) da pessoa capturada pela c�mera.
	//========================================================================================


	int im_width = images[0].cols;
	int im_height = images[0].rows;

	// cria um FaceRecognizer e treina o reconhecimento por sexo, idade e ID,
	//a partir das imagens carregadas do banco de imagens:
	modelIdade = createLBPHFaceRecognizer(1, 3, 6, 6, 20.0);
	cout << "modelo idade criado!" << endl;

	modelSexo = createLBPHFaceRecognizer(1, 3, 6, 6, 30.0);
	cout << "modelo sexo criado!" << endl;

	modelID = createLBPHFaceRecognizer(1, 3, 6, 6, 15.0);
	cout << "modelo ID criado!" << endl;

	//realizando aprendizagem de m�quina
	cout << "treinando Idade" << endl;
	modelIdade->train(images, labelsIdade);

	cout << "Treinando sexo" << endl;
	modelSexo->train(images, labelsSexo);

	cout << "treinando ID" << endl;
	modelID->train(images, labelsID);

}



//========================================================================================
//Metodos getets para retornar os modelos
//========================================================================================

Ptr<FaceRecognizer> LoadCSV::getModelIdade(){ return modelIdade; };
Ptr<FaceRecognizer> LoadCSV::getModelSexo(){ return modelSexo; };
Ptr<FaceRecognizer> LoadCSV::getModelID(){ return modelID; };