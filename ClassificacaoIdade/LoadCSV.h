#pragma once

#include "stdafx.h"
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "ShowVideo.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

using namespace std;

class LoadCSV
{


	Ptr<FaceRecognizer> modelIdade;
	Ptr<FaceRecognizer> modelSexo;
	Ptr<FaceRecognizer> modelID;

public:


	LoadCSV();
	Ptr<FaceRecognizer> getModelIdade();
	Ptr<FaceRecognizer> getModelSexo();
	Ptr<FaceRecognizer> getModelID();

};

